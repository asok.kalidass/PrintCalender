import java.util.*;
/**
 * <p>
 * Represents a month for a given year 
 * <p>
 */
public class Month
{
	//Declarations
	private String name;
	int[][] days;
	int daysInMonth;
	int firstDay;
	final String[] monthNames ={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	final String[] weekDays ={"S","M","T","W","T","F","SA"};
	int[] monthLengths = {31,28,31,30,31,30,31,31,30,31,30,31};
	final int weeksInMonth = 6;
	final int daysInWeek = 7;

	public String getName()
	{
		return name;
	}

	/**
	 * Constructor to initialize month object to fetch monthNames and weekDays
	 * <p>
	 */
	public Month()
	{

	}

	/**
	 * Constructor to initialize month object to fetch monthNames and weeks of month
	 * <p>
	 * @param year - the required year for which calendar has to be displayed.
	 * @param monthNum - the required month for which weeks has to be displayed.
	 */
	public Month(int year, int monthNum)
	{
		GregorianCalendar c = new GregorianCalendar(year, monthNum, 1); //GregorianCalendar object used to find what day the month starts on
		int currentDay = 1;

		//Set the name of the month
		name = monthNames[monthNum];

		// reset the length of February for leap years
		if (c.isLeapYear(year)) 
		{
			monthLengths[1] = 29; 
		}
		
		daysInMonth = monthLengths[monthNum];

		//What day of the week does the month start on?
		firstDay = c.get(Calendar.DAY_OF_WEEK) -1; 

		//Create an 2d array representing 6 weeks each of 7 days each
		days = new int[weeksInMonth][daysInWeek];  

		//Initialize the days array for this month
		for (int week = 0; week < weeksInMonth; week++)
		{ 
			for (int day = 0; day < daysInWeek; day++)
			{				
				if (!((week == 0 && day < firstDay) || (currentDay > daysInMonth)))
				{
					days[week][day] = currentDay++;
				}
			}
		}
	}
}