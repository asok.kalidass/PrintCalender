import java.util.ArrayList;
import java.util.Scanner;
/**
 * Represents a Calendar builder for a given year and displays it in a matrix form
 * <p>
 * Display of Calendar can have following rules
 * <ul>
 * <li>Rows - The number of rows needed to print a calendar for the given year
 * <li>Columns - The number of columns needed to print a calendar for the given year
 * </ul>
 * <p>
 * Assumptions/Restrictions: The user inputs the valid year ( Valid range is 1 � 3999 )
 * <p>
 * Noteworthy Features: Loading the month data for the required rows and then further rows data is loaded when needed
 * It is dynamic in nature that is rows and columns can be changed to print in required format
 * 
 * @author  Asok Kalidass Kalisamy (B00763356) - Graduate Student (with usage of month class by Dr. A. Rau-Chaplin with minor tweaking in array population)
 */
public class Main 
{
	//class variables
	static int batch = 0 ; // number of column segment
	static int count = 0;
	static int rows = 4; //No of rows
	static int columns = 3; //No of columns

	public static void main(String[] args) {
		
		Month monthData = new Month();
		@SuppressWarnings("resource")
		Scanner getYear = new Scanner(System.in);
		System.out.print("Enter the year");
		int year = getYear.nextInt();
		System.out.println(" ");
		System.out.println("The Calender for the year " + year + " is ");
		ArrayList<Month> monthList = new ArrayList<Month>();

		for (int rowCount = 0; rowCount < rows; rowCount++)
		{
			//batch + columns -> iterate till the column length ( each month is considered as set ) 
			for (int i = batch; i < (batch + columns); i++) 
			{
				monthData = new Month(year, i);
				monthList.add(monthData);
			}			
			PrintCalender(monthList);
		}
	}
	/**
	 * Prints the calendar
	 * <p>
	 * @param monthList holds month object at a time for required rows
	 */
	static void PrintCalender(ArrayList<Month> monthList)
	{
		batch += columns;
		System.out.println(" ");
		PrintHeader(monthList);
		System.out.println("");
		PrintDays(monthList);
		PrintRowSets(monthList);
		monthList.clear();
	}
	/**
	 * Prints the month name
	 * <p>
	 * @param monthList holds month object at a time for required rows
	 */
	static void PrintHeader(ArrayList<Month> monthList)
	{
		for (int arrayCount = 0; arrayCount < columns; arrayCount++)
		{
			System.out.print("            " + monthList.get(arrayCount).getName().toString() + "                ");
		}
	}
	/**
	 * Prints the days
	 * <p>
	 * @param monthList holds month object at a time for required rows
	 */
	static void PrintDays(ArrayList<Month> monthList)
	{
		//Get the months data for all the columns in a row
		for (int colCnt = 0; colCnt < columns; colCnt++)	//3 now of column 
		{
			for (int day = 0; day < 7; day++)	//7 days a week 
			{		
				System.out.print(monthList.get(0).weekDays[day]);
				System.out.print("   ");
			}
			System.out.print("   ");
		}
	}

	/**
	 * Prints the weeks starting from first week of each month for a row
	 * <p>
	 * @param monthList holds month object at a time for required rows
	 */
	static void PrintRowSets(ArrayList<Month> monthList)
	{
		//print the weeks for each of the month as per the given column
		for (int i = 0; i < 6; i++) //6 max rows can be possible for a week
		{			
			System.out.println("");			
			PrintColumnSets(monthList, i);
			count = 0;
		}		
	}

	/**
	 * Prints the days for each week 
	 * <p>
	 * @param monthList holds month object at a time for required rows
	 * @param i holds the current address of month object
	 */
	static void PrintColumnSets(ArrayList<Month> monthList, int i)
	{
		//print the first week of the month in each of the column
		for (int j = 0; j <= 7; j++)	//7 days a week
		{	
			//Give a spare upon printing first week data of a month
			if (j == 7)
			{
				count++;
				while(count < columns) // no of columns
				{
					System.out.print("    ");
					PrintColumnSets(monthList, i);
				}
				break;				
			}
			//Print the days and manage spacing between one digit and two digit number
			if (!(monthList.get(count).days[i][j] == 0))
			{
				System.out.print(monthList.get(count).days[i][j]);
				// this is to determine and give double spacing for numbers that has one char and single spacing for char that has two chars
				if (monthList.get(count).days[i][j]/2 <= 4.5 ) 
				{			
					System.out.print("   ");
				}
				else
				{
					System.out.print("  ");
				}
			}
			else
			{
				System.out.print("    ");
			}
		}
	}
}





